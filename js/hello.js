(function() {
  'use strict';

  var helloComponent = Vue.extend({
    template: '<p>Hello world</p>'
  });

  var vm = new Vue({
    el: '#app',
    components: {
      'hello-component': helloComponent
    }
  });
})();
